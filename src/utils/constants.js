// Header constant values
export const LOGO_FALLBACK = 'MIGHTY MINDS';
export const MAIN_NAV_LABELS = [
    {
        label: 'Home',
    },
    {
        label: 'Classes'
    },
    {
        label: 'Planner'
    },
    {
        label: 'School Data'
    },
    {
        label: 'Library'
    }
];
export const HELP_CENTRE_LABEL = 'Help Centre';
export const HELP_CENTRE_LINK = '#';
export const USER_SUB_HEADING_LABEL = '{userType} Account';
export const ACTIVE_WEEK_N = 4;
export const ACTIVE_WEEK_N_LABEL = 'WEEK {number} ACTIVITY SUMMARY';
export const WEEKS_ACTIVITY = {
    dueThisWeek: 330,
    completed: 240,
    overdue: 33
}
export const BANNER_HEADER = 'Welcome back, {name}';
export const DUE_THIS_WEEK_LABEL = 'Due this week: {number}';
export const COMPLETED_LABEL = 'Completed: {number}';
export const OVERDUE_LABEL = 'Overdue: {number}';
export const CALENDAR_LABEL = 'My Calendar';
export const WEEKLY_REPORT_LABEL = 'Weekly Report';
export const ASSIGN_ACTIVITY_LABEL = 'Assign Activity';

// User sample values to be changed when backend is implemented
export const USER_FIRST_NAME = 'Remo';
export const USER_LAST_NAME = 'Lalata';
export const USER_TYPE = 'Teacher';

// Card classes sample values
export const CLASSES_CONTAINER_TITLE = 'Here are your classes';
export const CLASSES_CONTAINER_SUB_TITLE = "Select a class to view this week's assigned activities and begin your lesson";
export const CLASSES_VIEW_ALL_LABEL = 'View all classes';
export const CLASSES = [
    {
        course: '12ENGA',
        subject: 'English',
        year: 12,
        activity_this_week: 3
    },
    {
        course: '12ENGB',
        subject: 'English',
        year: 12,
        activity_this_week: 0
    },
    {
        course: '08MATHS',
        subject: 'Maths',
        year: 8,
        activity_this_week: 3
    },
    {
        course: '09SCI',
        subject: 'Science',
        year: 9,
        activity_this_week: 6
    },
    {
        course: '09HASS',
        subject: 'Humanities and Social Science',
        year: 1,
        activity_this_week: 1
    },
    {
        course: '09HASS',
        subject: 'Humanities and Social Science',
        year: 1,
        activity_this_week: 1
    },
    {
        course: '09HASS',
        subject: 'Humanities and Social Science',
        year: 1,
        activity_this_week: 1
    }
];
export const YEAR_LABEL = 'Year {year}';
export const CLASSES_CONTENT_LABELS = ['Activity due this week', 'Assign Activities', 'Class calendar']

// Sidebar sample values
export const GUIDE_TITLE = 'Explore your new portal';
export const GUIDE_SUB_TITLE = 'Improve class focus, unit plans, live lessons, additional tracking features and much more.';
export const GUIDE_BUTTON_LABEL = 'See how it works';
export const GUIDE_LINK_LABEL = 'Getting started guide';
export const GUIDE_LINK = '#';
export const HELP_SUPPORT_LABEL = 'Help & Support';
export const HELP_SUPPORT_LINKS = [
    {
        label: 'Visit help centre',
        link: '#'
    },
    {
        label: 'Send us your feedback',
        link: '#'
    },
    {
        label: 'Make a request or suggestion',
        link: '#'
    },
    {
        label: 'Report an issue',
        link: '#'
    },
    {
        label: 'Teacher support group',
        link: '#'
    },
    {
        label: 'Schedule a consultation',
        link: '#'
    },
]