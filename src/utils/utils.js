export const validateObj = obj => {
    if (Object.keys(obj).length) {
        return true;
    }

    return false;
}

export const getInitials = string => {
    if (string) {
        let stringToArr = string.split(' ');
        let spliceFirstCharacter = stringToArr.map(x => x.charAt(0));
        return spliceFirstCharacter.join('');
    }

    return false;
}

export const hexToRgbA = (hex, opacity = 1) => {
    let c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
        c = hex.substring(1).split('');
        if (c.length === 3) {
            c = [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c = '0x' + c.join('');
        return `rgba(${[(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',')}, ${opacity})`;
    }

    return 'linear-gradient(180deg, rgba(0,255,0,0.2) 0%, rgba(255,255,255,0.01) 100%)';
}

export const getObjVal = (obj, key) => {
    if (validateObj(obj) && obj.hasOwnProperty(key)) {
        return obj[key];
    }

    return '';
}

export const validateHex = hex => /^#([0-9a-f]{3}){1,2}$/i.test(hex);