import { Box, Typography, Divider, styled } from '@mui/material';
import OfflineBoltRoundedIcon from '@mui/icons-material/OfflineBoltRounded';
import StarRateRoundedIcon from '@mui/icons-material/StarRateRounded';

import { YEAR_LABEL } from '../../../utils/constants';

const CustomBoltIcon = styled(OfflineBoltRoundedIcon)(({ bgcolor }) => ({
    color: '#fff',
    backgroundColor: bgcolor,
    borderRadius: '4px',
    width: '20px',
    height: '20px',
    marginRight: '10px'
}));

const CardHeader = props => {
    const year = props.year && props.year !== '' ? YEAR_LABEL.replace('{year}', props.year) : props.year;

    return (
        <Box sx={{ mt: 2, ml: 2, mr: 2 }}>
            <Box
                sx={{
                    display: 'flex',
                    alignItems: 'center'
                }}
            >
                <CustomBoltIcon bgcolor={props.bgcolor} />
                <Typography variant='h6' fontWeight='bold' lineHeight='normal'>{props.course}</Typography>
            </Box>
            <Box
                sx={{
                    display: 'flex',
                    alignItems: 'center',
                    mt: 1,
                    '& .MuiSvgIcon-root': {
                        color: '#3972f5',
                        fontSize: '18px',
                        position: 'relative',
                        top: '-1px'
                    },
                    '& .MuiTypography-root': {
                        fontSize: '14px',
                        position: 'relative',
                        top: '1px',
                        whiteSpace: 'nowrap'
                    },
                    '& hr': {
                        margin: '2px 12px'
                    }
                }}
            >
                <StarRateRoundedIcon />
                <Divider orientation='vertical' flexItem />
                <Typography>{year}</Typography>
                <Divider orientation='vertical' flexItem />
                <Typography overflow='hidden' whiteSpace='nowrap' textOverflow='ellipsis'>{props.subject}</Typography>
            </Box>
        </Box>
    )
}

export default CardHeader;