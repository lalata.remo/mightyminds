import { 
    List, 
    ListItem, 
    ListItemButton, 
    ListItemText, 
    Chip
} from '@mui/material';

import { CLASSES_CONTENT_LABELS } from '../../../utils/constants';

const CardBody = props => {
    return (
        <List
            sx={{
                mt: 0.5,
                '& .MuiListItemButton-gutters': {
                    py: 0.3
                },
                '& .MuiListItemText-primary': {
                    fontSize: '14px'
                },
                '& .MuiChip-root': {
                    width: '40px',
                    borderRadius: '4px'
                }
            }}
        >
            {CLASSES_CONTENT_LABELS.map((item, i) =>
                <ListItem
                    disablePadding
                    key={i}
                    secondaryAction={i === 0 ? <Chip label={props.activity_this_week} size='small' /> : null}
                >
                    <ListItemButton>
                        <ListItemText primary={item} />
                    </ListItemButton>
                </ListItem>
            )}

        </List>
    )
}

export default CardBody;