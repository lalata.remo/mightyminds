import { CardActions, Button, Typography } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';

const CardFooter = () => {
    return (
        <CardActions>
            <Button
                sx={{
                    opacity: '0.5',
                    '& .MuiTypography-root, & .MuiSvgIcon-root': {
                        color: '#4f595f',
                        fontSize: '13px'
                    },
                    '& .MuiTypography-root': {
                        position: 'relative',
                        top: '1px',
                        ml: 1,
                        textTransform: 'none'
                    }
                }}
            >
                <AddIcon />
                <Typography>Add Student</Typography>
            </Button>
        </CardActions>
    )
}

export default CardFooter;