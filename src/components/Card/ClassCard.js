import {
    Card,
    styled,
    Divider
} from '@mui/material';
import randomcolor from 'randomcolor';

import CardHeader from './CardHeader/CardHeader';
import CardBody from './CardBody/CardBody';
import CardFooter from './CardFooter/CardFooter';

import { hexToRgbA, getObjVal, validateHex } from '../../utils/utils';

const CustomOverlay = styled(Card)(({ bgtopcolor, bgcolor }) => ({
    '&::before': {
        content: '""',
        position: 'absolute',
        right: '0',
        left: '0',
        top: '1px',
        height: '80px',
        background: `linear-gradient(180deg, ${bgcolor} 0%, rgba(255,255,255,0.01) 100%)`,
        borderTop: `4px solid ${bgtopcolor}`
    }
}));

const ClassCard = props => {
    
    let randomHex = props.theme && validateHex(props.theme) ? props.theme : '#ade283';
    randomHex = props.randomTheme ? randomcolor() : randomHex;
    const randomBgColor = hexToRgbA(randomHex, '0.2');

    const course = getObjVal(props.data, 'course');
    const subject = getObjVal(props.data, 'subject');
    const year = getObjVal(props.data, 'year');
    const activity_this_week = getObjVal(props.data, 'activity_this_week');

    return (
        <Card sx={{ position: 'relative', borderTopLeftRadius: '0', borderTopRightRadius: '0' }}>
            <CustomOverlay bgtopcolor={randomHex} bgcolor={randomBgColor} />
            <CardHeader
                bgcolor={randomHex}
                course={course}
                subject={subject}
                year={year}
            />
            <CardBody activity_this_week={activity_this_week} />
            <Divider sx={{ ml: 2, mr: 2 }} />
            <CardFooter />
        </Card>
    )
}

export default ClassCard;