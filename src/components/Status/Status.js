import { Box, Typography } from '@mui/material';
import CircleIcon from '@mui/icons-material/Circle';

const Status = props => {

    const label = props.label ? props.label : null;
    const color = props.status && props.status === 'success' ? 'green' : props.status === 'danger' ? 'red' : 'blue';

    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                '& .MuiSvgIcon-root': {
                    fontSize: '9px',
                    position: 'relative',
                    bottom: '1px'
                }
            }}
        >
            <CircleIcon sx={{ color: color }} />
            {label && <Typography ml={0.5} fontSize='12px' color='white' sx={{ opacity: '0.8'}}>{label}</Typography>}
        </Box>
    )
}

export default Status;