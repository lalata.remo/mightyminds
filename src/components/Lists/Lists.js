import {
    Typography,
    Paper,
    List,
    ListItem,
    ListItemButton,
    ListItemIcon,
    ListItemText,
    Divider
} from '@mui/material';

const Lists = props => {

    const title = props.title ? props.title : null;
    const items = props.items ? props.items : null;
    const dividerPosition = props.dividerPosition ? props.dividerPosition : null;

    return (
        <Paper
            sx={{
                p: 2,
                pb: 0,
                '& .MuiSvgIcon-root': {
                    color: '#b2bcd0'
                }
            }}
        >
            {title &&
                <Typography
                    fontSize={15}
                    fontWeight='bold'
                    sx={{
                        mb: 2,
                        '& .MuiSvgIcon-root': {
                            fontSize: '16px',
                            position: 'relative',
                            top: '2px',
                            mr: 0.5
                        }
                    }}
                >
                    {props.titleIcon && props.titleIcon}
                    {title}
                </Typography>
            }

            <Divider />

            {items.length &&
                <List
                    sx={{
                        py: 1,
                        '& .MuiListItemButton-root': {
                            p: 0.5
                        },
                        '& .MuiTypography-root': {
                            fontSize: '14px'
                        },
                        '& .MuiListItemIcon-root': {
                            minWidth: '40px'
                        },
                        ...(dividerPosition !== null && {[`& .MuiListItem-root:nth-of-type(${dividerPosition})`]: {
                            mb: 2
                        }}),
                        ...(dividerPosition !== null && {[`& .MuiListItem-root:nth-of-type(${dividerPosition+1})`]: {
                            mt: 2
                        }}),
                        ...(dividerPosition !== null && {[`& .MuiListItem-root:nth-of-type(${dividerPosition+1})::before`]: {
                            content: '""',
                            height: '1px',
                            bgcolor: 'rgba(0, 0, 0, 0.12)',
                            position: 'absolute',
                            top: '-0.5rem',
                            left: '-16px',
                            right: '-16px',
                        }})
                    }}
                >
                    {items.map(item =>
                        <ListItem disablePadding>
                            <ListItemButton component='a' href={item.link}>
                                {props.itemIcon && <ListItemIcon>
                                    {props.itemIcon}
                                </ListItemIcon>}
                                <ListItemText primary={item.label} />
                            </ListItemButton>
                        </ListItem>
                    )}
                </List>
            }
        </Paper>
    )
}

export default Lists;