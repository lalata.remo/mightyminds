import {
    Paper,
    Box,
    Container,
    Typography,
    ButtonGroup,
    Button
} from '@mui/material';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import AddIcon from '@mui/icons-material/Add';

import Status from '../Status/Status';

import {
    ACTIVE_WEEK_N_LABEL,
    DUE_THIS_WEEK_LABEL,
    OVERDUE_LABEL,
    COMPLETED_LABEL,
    BANNER_HEADER,
    ASSIGN_ACTIVITY_LABEL,
    CALENDAR_LABEL,
    WEEKLY_REPORT_LABEL
} from '../../utils/constants';

import GlobeBooks from '../../assets/images/globe_books.png';

const Banner = props => {
    return (
        <Paper
            sx={{
                py: 5,
                bgcolor: '#0c224e',
                backgroundImage: {
                    md: `url(${GlobeBooks})`,
                    xs: 'none'
                },
                backgroundRepeat: 'no-repeat',
                backgroundSize: '7%',
                backgroundPosition: '5% bottom',

            }}
            square
        >
            <Container
                sx={{
                    maxWidth: {
                        md: '70%'
                    },
                    display: 'flex',
                    flexDirection: {
                        md: 'row',
                        xs: 'column'
                    },
                    alignItems: 'center',
                    justifyContent: 'space-between'
                }}
            >
                <Box>
                    <Box>
                        {props.userData && props.userData.name &&
                            <Typography
                                variant='h5'
                                fontWeight='bold'
                                color='white'
                                letterSpacing='1px'
                                mb={4}
                            >{BANNER_HEADER.replace('{name}', props.userData.name)}</Typography>}

                        {props.userData.activeWeek &&
                            <Typography
                                color='#7988a5'
                                fontSize='12px'
                                fontWeight='bold'
                                letterSpacing='1px'
                                mb={1}
                            >{ACTIVE_WEEK_N_LABEL.replace('{number}', props.userData.activeWeek)}</Typography>}
                    </Box>
                    <Box
                        sx={{
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            width: {
                                md: '350px'
                            }
                        }}
                    >
                        {/* status reusable component status: success|danger, default is blue */}

                        {
                            props.userData &&
                            props.userData.weeksActivity &&
                            props.userData.weeksActivity.dueThisWeek &&
                            <Status label={DUE_THIS_WEEK_LABEL.replace('{number}', props.userData.weeksActivity.dueThisWeek)} />
                        }
                        {
                            props.userData &&
                            props.userData.weeksActivity &&
                            props.userData.weeksActivity.completed &&
                            < Status label={COMPLETED_LABEL.replace('{number}', props.userData.weeksActivity.completed)} status='success' />
                        }
                        {
                            props.userData &&
                            props.userData.weeksActivity &&
                            props.userData.weeksActivity.overdue &&
                            <Status label={OVERDUE_LABEL.replace('{number}', props.userData.weeksActivity.overdue)} status='danger' />
                        }
                    </Box>
                </Box>
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: {
                            md: 'row',
                            xs: 'column'
                        },
                        width: {
                            md: 'auto',
                            xs: '100%'
                        },
                        mt: {
                            xs: 4,
                            md: 0
                        },
                        gap: '15px',
                        '& .MuiButton-root': {
                            fontSize: '0.8rem',
                            textTransform: 'none',
                            display: 'block',
                            letterSpacing: '1px',
                            height: '35px'
                        },
                        '& .MuiButtonGroup-grouped': {
                            px: 1,
                            '& p': {
                                textAlign: 'center'
                            }
                        },
                        '& .MuiButtonGroup-grouped:first-of-type': {
                            flexGrow: {
                                xs: 1
                            },
                        },
                        '& .MuiButton-outlined': {
                            borderColor: 'white',
                            color: 'white'
                        }
                    }}
                >
                    <Button variant='outlined' disableElevation>{CALENDAR_LABEL}</Button>
                    <Button variant='outlined' disableElevation>{WEEKLY_REPORT_LABEL}</Button>
                    <ButtonGroup
                        variant='contained'
                        disableElevation
                        sx={{
                            '& p .MuiSvgIcon-root': {
                                fontSize: '15px',
                                position: 'relative',
                                bottom: '1px'
                            },
                            '& .MuiButtonGroup-grouped:not(:last-of-type)': {
                                borderRight: '4px solid #0c224e'
                            }
                        }}
                    >
                        <Button>
                            <Typography display='flex' alignItems='center' fontSize='0.8rem'>
                                <AddIcon sx={{ fontSize: '1.2rem' }} />
                                {ASSIGN_ACTIVITY_LABEL}
                            </Typography>
                        </Button>
                        <Button>
                            <KeyboardArrowDownIcon />
                        </Button>
                    </ButtonGroup>
                </Box>
            </Container>
        </Paper>
    )
}

export default Banner;