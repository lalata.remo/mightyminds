import { Box, Paper, Typography, Button, Link } from '@mui/material';

import Student from '../../assets/images/student_laptop.png';

import {
    GUIDE_TITLE,
    GUIDE_SUB_TITLE,
    GUIDE_BUTTON_LABEL,
    GUIDE_LINK_LABEL,
    GUIDE_LINK
} from '../../utils/constants';

const Guide = () => {
    return (
        <Paper
            sx={{
                p: 2,
                mb: 4,
                bgcolor: '#d2dfff',
                '& .MuiButton-contained': {
                    textTransform: 'none',
                    mt: 1,
                    mb: 2
                },
                '& .MuiLink-root': {
                    fontSize: '14px'
                }
            }}
        >
            <Box
                sx={{
                    '& img': {
                        maxWidth: '100%',
                        display: 'block',
                        mt: '-50px',
                        mb: 2
                    }
                }}
            >
                <img src={Student} alt={GUIDE_TITLE} />
            </Box>
            <Typography fontWeight='bold' fontSize='16px'>{GUIDE_TITLE}</Typography>
            <Typography fontSize='14px' mt={1} mb={1.5}>{GUIDE_SUB_TITLE}</Typography>
            <Button fullWidth variant='contained'>{GUIDE_BUTTON_LABEL}</Button>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center'
                }}
            >
                <Link href={GUIDE_LINK} >{GUIDE_LINK_LABEL}</Link>
            </Box>
        </Paper>
    )
}

export default Guide;