import { IconButton, Menu, MenuItem, Typography } from '@mui/material';
import { Menu as MenuIcon } from '@mui/icons-material';

const MobileMenu = props => {
    return (
        <>
            <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={props.onMenuOpen}
                color="inherit"
            >
                <MenuIcon sx={{ color: 'white' }} />
            </IconButton>
            <Menu
                id="menu-appbar"
                anchorEl={props.anchorElNav}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                open={Boolean(props.anchorElNav)}
                onClose={props.onMenuClose}
                sx={{
                    display: { xs: 'block', md: 'none' },
                }}
            >
                {props.menuData.map(nav => (
                    <MenuItem key={nav.label} onClick={props.onMenuClose}>
                        <Typography textAlign="center">{nav.label}</Typography>
                    </MenuItem>
                ))}
            </Menu>
        </>
    )
}

export default MobileMenu;