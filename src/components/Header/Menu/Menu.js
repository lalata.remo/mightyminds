import { Box, styled } from '@mui/material';

import DesktopMenu from './DesktopMenu/DesktopMenu';
import MobileMenu from './MobileMenu/MobileMenu';

const CustomDesktopBox = styled(Box)({
    marginLeft: '30px',
    '& button:first-of-type::before': {
        content: '""',
        width: '1px',
        position: 'absolute',
        top: '5px',
        left: '-30px',
        bottom: '5px',
        backgroundColor: 'rgba(255, 255, 255, 0.1)'
    }
});

const Menu = props => {
    return (
        <>
            <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                <MobileMenu
                    menuData={props.data}
                    onMenuOpen={props.handleOpenNavMenu}
                    onMenuClose={props.handleCloseNavMenu}
                    anchorElNav={props.anchorElNav}
                />
            </Box>
            <CustomDesktopBox sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                <DesktopMenu menuData={props.data} />
            </CustomDesktopBox>
        </>
    )
}

export default Menu;