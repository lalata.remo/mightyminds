import { Button, styled } from '@mui/material';

const CustomButtom = styled(Button)({
    textTransform: 'none',
    fontSize: '1rem',
    fontWeight: '700',
    marginTop: '16px',
    marginBottom: '16px',
    color: '#fff',
    display: 'block',
    opacity: '0.5',
    marginRight: '15px',
    marginLeft: '16px',
    padding: '6px 0',
    minWidth: 'auto',
    '&.active': {
        opacity: '1',
        '&::after': {
            content: '""',
            position: 'absolute',
            bottom: '2px',
            left: '0',
            right: '0',
            height: '2px',
            backgroundColor: '#3972f5'
        }
    },
    '&:hover': {
        opacity: '1'
    }
});

const DesktopMenu = props => {
    return (
        <>
            {props.menuData.map(nav => (
                <CustomButtom
                    key={nav.label}
                    className={nav.active ? 'active' : ''}
                >
                    {nav.label}
                </CustomButtom>
            ))}
        </>
    )
}

export default DesktopMenu;