import { Box, styled } from '@mui/material';

import MightyMindsLogo from '../../../assets/images/logo.png';

const CustomImage = styled('img')({
    maxWidth: '100%',
    display: 'block'
});

const Logo = () => {
    return (
        <Box sx={{ maxWidth: 200 }}>
            <CustomImage src={MightyMindsLogo} />
        </Box>
    )
}

export default Logo;