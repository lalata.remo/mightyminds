import {
    Box,
    Avatar as MUIAvatar,
    styled
} from '@mui/material';

import { getInitials } from '../../../utils/utils';
import { USER_SUB_HEADING_LABEL } from '../../../utils/constants';

const CustomAvatarFallback = styled('span')({
    textTransform: 'uppercase'
});

const CustomTypography = styled('div')({
    fontWeight: '700',
    fontSize: '0.8rem',
    letterSpacing: '1px',
    color: '#fff',
    '&.sub': {
        opacity: '0.5'
    }
});

const Avatar = props => {

    const image = props.images;
    const name = props.name;
    const userType = props.type;
    const subHeading = USER_SUB_HEADING_LABEL.replace('{userType}', userType);

    return (
        <Box
            sx={{
                display: 'flex',
                flexGrow: 0,
                alignItems: 'center'
            }}>
            <Box
                sx={{
                    textAlign: 'right',
                    mr: 1,
                    display: {
                        md: 'block',
                        xs: 'none'
                    }
                }}
            >
                <CustomTypography>{name}</CustomTypography>
                <CustomTypography className='sub'>{subHeading}</CustomTypography>
            </Box>
            <MUIAvatar
                alt={name}
                src={image}
                variant='rounded'
                sx={{ bgcolor: '#8660f1' }}
            >
                <CustomAvatarFallback>
                    {getInitials(name)}
                </CustomAvatarFallback>
            </MUIAvatar>
        </Box>
    )
}

export default Avatar;