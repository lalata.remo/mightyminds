import React, { useState, useEffect } from 'react';
import {
    AppBar,
    Box,
    Toolbar,
    Typography,
    Container,
    IconButton,
    Link,
    Paper
} from '@mui/material';
import Help from '@mui/icons-material/Help';

import Logo from './Logo/Logo';
import Menu from './Menu/Menu';
import Avatar from './Avatar/Avatar';

import { validateObj } from '../../utils/utils';
import { MAIN_NAV_LABELS, HELP_CENTRE_LABEL, HELP_CENTRE_LINK } from '../../utils/constants';

const Header = props => {
    const [anchorElNav, setAnchorElNav] = useState(null);
    const [userName, setUserName] = useState('');
    const [userType, setUserType] = useState('');
    const [navItems, setNavItems] = useState([]);

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    useEffect(() => {
        if (validateObj(props.userData)) {
            setUserName(props.userData && props.userData.name ? props.userData.name : null);
            setUserType(props.userData && props.userData.userType ? props.userData.userType : null);
        }
    }, [props.userData]);

    useEffect(() => {
        if (MAIN_NAV_LABELS.length) {
            let items = MAIN_NAV_LABELS;
            items = items.map((x, i) => {

                if (i === 0) {
                    return {
                        ...x,
                        label: x.label,
                        active: true
                    }
                }

                return {
                    ...x,
                    label: x.label,
                    active: false
                }
            });

            setNavItems(items);
        }
    }, []);

    return (
        <AppBar position='static' >
            <Paper sx={{ bgcolor: '#0f2551' }} elevation={3} square>
                <Container sx={{ minWidth: { md: '100%' } }}>
                    <Toolbar disableGutters>
                        <Box
                            sx={{
                                display: {
                                    xs: 'none',
                                    md: 'block'
                                }
                            }}
                        >
                            <Logo />
                        </Box>

                        {navItems && navItems.length &&
                            <Menu
                                data={navItems}
                                handleOpenNavMenu={handleOpenNavMenu}
                                handleCloseNavMenu={handleCloseNavMenu}
                                anchorElNav={anchorElNav}
                            />
                        }

                        <Box
                            sx={{
                                display: {
                                    xs: 'block',
                                    md: 'none'
                                },
                                flexGrow: 1
                            }}
                        >
                            <Logo />
                        </Box>

                        <Box
                            sx={{
                                marginRight: {
                                    md: '61px',
                                    xs: 1
                                },
                                position: 'relative',
                                '&::after': {
                                    content: '""',
                                    width: '1px',
                                    position: 'absolute',
                                    right: '-26px',
                                    top: '0',
                                    bottom: '0',
                                    backgroundColor: '#fff',
                                    opacity: '0.1'
                                }
                            }}
                        >
                            <Link href={HELP_CENTRE_LINK} underline='none'>
                                <IconButton>
                                    <Help sx={{ color: '#fff', opacity: '0.5', fontSize: '1rem', mr: 0.5 }} />
                                    <Typography
                                        sx={{
                                            color: '#fff',
                                            opacity: '0.5',
                                            fontWeight: '700',
                                            display: {
                                                md: 'block',
                                                xs: 'none'
                                            }
                                        }}
                                        variant='body2'
                                    >
                                        {HELP_CENTRE_LABEL}
                                    </Typography>
                                </IconButton>
                            </Link>
                        </Box>

                        <Avatar name={userName} type={userType} />
                    </Toolbar>
                </Container>
            </Paper>
        </AppBar >
    );
};
export default Header;