import React, { useState, useEffect } from 'react';
import {
    Box,
    Container,
    Grid,
    Typography,
    Link,
    Button
} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import HelpRoundedIcon from '@mui/icons-material/HelpRounded';
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';

import Header from '../../components/Header/Header';
import Banner from '../../components/Banner/Banner';
import Card from '../../components/Card/ClassCard';
import Lists from '../../components/Lists/Lists';
import Guide from '../../components/Guide/Guide';

import {
    USER_FIRST_NAME,
    USER_LAST_NAME,
    USER_TYPE,
    ACTIVE_WEEK_N,
    CLASSES,
    CLASSES_VIEW_ALL_LABEL,
    CLASSES_CONTAINER_SUB_TITLE,
    CLASSES_CONTAINER_TITLE,
    HELP_SUPPORT_LABEL,
    HELP_SUPPORT_LINKS,
    WEEKS_ACTIVITY
} from '../../utils/constants';

const Home = () => {
    const [userData, setUserData] = useState({});
    const [classes, setClasses] = useState([]);

    // set values on component initial load
    useEffect(() => {
        setUserData(prevState => {
            return {
                ...prevState,
                name: `${USER_FIRST_NAME} ${USER_LAST_NAME}`,
                userType: USER_TYPE,
                activeWeek: ACTIVE_WEEK_N,
                weeksActivity: WEEKS_ACTIVITY
            }
        });

        setClasses(CLASSES.slice(0, 5));
    }, []);

    return (
        <Box
            bgcolor='#f8f9fc'
            pb={5}
        >
            <Header userData={userData} />
            <Banner userData={userData} />
            <Container
                sx={{
                    maxWidth: {
                        md: '70%'
                    }
                }}
            >
                <Grid container columnSpacing={8} rowSpacing={4} mt={ {xs: 0, md: 5} }>
                    <Grid
                        item
                        md={9}
                        xs={12}
                        sx={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'flex-end',
                            '& .MuiTypography-body1, & .MuiLink-root': {
                                color: '#4f595f'
                            },
                            '& .MuiLink-root': {
                                cursor: 'pointer',
                                textDecorationColor: '#4f595f'
                            }
                        }}
                    >
                        <Box>
                            <Typography variant='h5' fontWeight='bold'>{CLASSES_CONTAINER_TITLE}</Typography>
                            <Typography>{CLASSES_CONTAINER_SUB_TITLE}</Typography>
                        </Box>
                        <Box
                            sx={{
                                display: {
                                    xs: 'none',
                                    md: 'block'
                                }
                            }}
                        >
                            <Link>
                                {CLASSES_VIEW_ALL_LABEL}
                            </Link>
                        </Box>
                    </Grid>
                    <Grid item md={9} xs={12}>
                        {classes.length && <Grid container spacing={4}>
                            {classes.map((item, i) =>
                                <Grid item md={4} xs={12} key={i}>
                                    {/* classes cards reusable component */}
                                    <Card
                                        data={item}
                                        // theme='#0f2551' // tpye: hex, theme of the card, default is #ade283
                                        randomTheme={true} // set true to generate random themes
                                    />
                                </Grid>
                            )}
                            <Grid
                                item
                                md={4}
                                sx={{
                                    display: {
                                        xs: 'none',
                                        md: 'block'
                                    }
                                }}
                            >
                                <Button
                                    variant='outlined'
                                    sx={{
                                        border: '3px dashed #ebebeb',
                                        width: '100%',
                                        height: '100%',
                                        '&:hover': {
                                            border: '3px dashed #ebebeb'
                                        }
                                    }}
                                >
                                    <AddIcon sx={{ fontSize: '3.5rem' }} color='disabled' />
                                </Button>
                            </Grid>

                            <Grid
                                item
                                xs={12}
                                sx={{
                                    display: {
                                        xs: 'block',
                                        md: 'none'
                                    },
                                    textAlign: 'center',
                                    py: 2
                                }}
                            >
                                <Box>
                                    <Link>
                                        {CLASSES_VIEW_ALL_LABEL}
                                    </Link>
                                </Box>
                            </Grid>
                        </Grid>}
                    </Grid>
                    <Grid
                        item
                        md={3}
                        xs={12}
                        sx={{
                            mt: {
                                xs: 4,
                                md: 0
                            }
                        }}
                    >

                        <Guide />

                        <Lists // reusable component for displaying a list of links
                            title={HELP_SUPPORT_LABEL}
                            items={HELP_SUPPORT_LINKS}
                            titleIcon={<HelpRoundedIcon />} // leave blank if title title not needed
                            itemIcon={<ArrowRightAltIcon />} // leave blank if icon item on list not needed
                            dividerPosition={4} // divider on the list, leave blank if not needed
                        />
                    </Grid>
                </Grid>
            </Container>
        </Box>
    )
}

export default Home;